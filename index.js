var fs = require('hexo-fs');
var path = require('path');
var ejs = require('ejs');

hexo.extend.helper.register('hexo_autogallery_helper', function(post_assets_path, dir_name, layout, small_prefix){

  var dir = path.join(hexo.source_dir,'_posts', dir_name);
  var themePath = path.join(hexo.theme_dir, 'layout', '_partial', layout + '.ejs');
  var relativeDir = path.join('_posts', dir_name);
  var photos_names = [];
  var small_photos = [];

  try {
    var files = fs.listDirSync(dir);
    var template = fs.readFileSync(themePath);

    var photos = files.map(f => relativeDir + '/' + f);

    console.log("[Autogallery helper] Rendered photos:");
    console.time('Rendered in:');

    for (const photo in photos) {
        photos_names[photo] = photos[photo].substring(7 + dir_name.length + 1);       
        photos[photo] = post_assets_path + photos_names[photo];
        small_photos[photo] = post_assets_path + small_prefix + photos_names[photo];
    }
    console.log(photos);
    console.timeEnd('Rendered in:');

    return ejs.render(template, {
      photos: photos,
      config: hexo.config,
      small_photos: small_photos
    })

  } catch (e) {
    console.error(e);
    return '<h3>Could not render autogallery at ' + post_assets_path + ' using ' + layout + ' layout</h3>'
  }
});
